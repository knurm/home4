/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */

public class Lfraction implements Comparable<Lfraction> {

    /** Main method. Different tests. */
    public static void main(String[] param) {
        //Your debugging tests here
    }

    // instance variables here
    private long numerator;
    private long denominator;

    /** Constructor.
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        numerator = a;
        if (b == 0) {
        	throw new RuntimeException("Murru nimetaja ei saa olla null!");
        } else {
        	denominator = b;
        }
    }

    /** Public method to access the numerator field.
     * @return numerator
     */
    public long getNumerator() {
        return this.numerator;
    }

    /** Public method to access the denominator field.
     * @return denominator
     */
    public long getDenominator() {
        return this.denominator;
    }

    /** Conversion to string.
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        String toString;
        long integerPart = this.integerPart();
    	if (integerPart == 0) {
    		toString = this.numerator + "/" + this.denominator;
    		return toString;
        } else {
        	toString = integerPart + " " + this.numerator + "/" + this.denominator;
        	return toString;
        }
    }

    /** Equality test.
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        Lfraction otherFraction = (Lfraction) m;
        return this.numerator * otherFraction.denominator == otherFraction.numerator * this.denominator;
    }

    /** Hashcode has to be equal for equal fractions.
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    /** Sum of fractions.
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        if (this.denominator == m.denominator) {
            return new Lfraction(this.numerator + m.numerator, this.denominator);
        } else {
            return new Lfraction((m.numerator*this.denominator)+(this.numerator*m.denominator), (m.denominator*this.denominator));
        }
    }

    /** Multiplication of fractions.
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
    	//long numer = this.numerator * m.numerator;
    	//long denom = this.denominator * m.denominator;
        return new Lfraction(this.numerator * m.numerator, this.denominator * m.denominator).reduce();
        //
    }

    /** Inverse of the fraction. n/d becomes d/n.
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        long numer = this.denominator;
        long denom = this.numerator;
        if (numer == 0) {
            throw new ArithmeticException();
        } else {
            if (denom <= 0) {
                numer = -numer;
                denom = -1 * denom;
            }
            return new Lfraction(numer, denom);
        }
    }

    /** Opposite of the fraction. n/d becomes -n/d.
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        //long numer = -this.numerator;
    	//long denom = this.denominator;
        return new Lfraction(-this.numerator, this.denominator);
    }

    /** Difference of fractions.
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
    	long gCD = greatestCommonDivisor(this.denominator, m.denominator);
    	long numer;
    	long denom;
    	if (gCD == 1) {
    		numer = (this.denominator*m.numerator)-(m.numerator*this.denominator);
    		denom = this.denominator*m.denominator;
    		return new Lfraction(numer, denom);
    	} else {
        	if (this.denominator > m.denominator) {
        		denom = this.denominator;
        		numer = this.numerator*(m.denominator/gCD)-m.numerator*(this.denominator/gCD);
        	} else {
        		denom = m.denominator;
        		numer = this.numerator*(m.denominator/gCD) - m.numerator*(this.denominator/gCD);
        	}
            return new Lfraction(numer, denom);
    	}
    }

    /** Quotient of fractions.
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
    	if (this.denominator < 0 || m.denominator < 0) {
            throw new ArithmeticException();
        } else {
            return new Lfraction(this.numerator*m.denominator, this.denominator*m.numerator).reduce();
        }
    }

    /** Comparision of fractions.
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        if (this.equals(m)) {
            return 0;
        } else if (this.numerator * m.denominator  < m.numerator * this.denominator) {
            return -1;
        } else {
            return 1;
        }
    }

    /** Clone of the fraction.
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
    	return new Lfraction(numerator, denominator);
    }

    /** Integer part of the (improper) fraction.
     * @return integer part of this fraction
     */
    public long integerPart() {
        return (numerator-(this.numerator%this.denominator))/this.denominator;
    }

    /** Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
    	//long numer = this.numerator;
    	//long denom = this.denominator;
    	long numer;
    	
    	if (Math.abs(this.numerator) > this.denominator) {
    		numer = this.numerator%this.denominator;
        	return new Lfraction(numer, this.denominator);
        } else {
        	return new Lfraction(this.numerator, this.denominator);
        }
    }

    /** Approximate value of the fraction.
     * @return numeric value of this fraction
     */
    public double toDouble() {
    	double numer = (double) this.numerator;
    	double denom = (double) this.denominator;
    	return numer/denom;
    }

    /** Double value f presented as a fraction with denominator d > 0.
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        return new Lfraction(Math.round(f * d), d);
    }

    /** Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
    	long numer;
    	long denom;
    	long integerPart;
        if (s.matches("[^~#@*+%{}<>\\[]|\"\\_^]*")) {
            throw new RuntimeException("String contains illegal characters " + s);
        }
    	if (!s.contains(" ")) {
    		String[] separate = s.split("/");
    		numer = Long.parseLong(separate[0]);
    		denom = Long.parseLong(separate[1]);
    	} else {
    		String[] separate1 = s.split(" ");
    		integerPart = Long.parseLong(separate1[0]);
    		String[] separate2 = separate1[1].split("/");
    		denom = Long.parseLong(separate2[1]);
    		numer = Long.parseLong(separate2[0])+(denom*integerPart);
    	}
    	return new Lfraction(numer, denom);
    }
    
    public static long greatestCommonDivisor(long a, long b) {
    	if (b == 0) {
    		return a; 
    	} else {
    		return greatestCommonDivisor(b, a%b);
    	}
    }
    
    public Lfraction reduce() {
    	long gCD = greatestCommonDivisor(this.numerator, this.denominator);
    	long numer = this.numerator / gCD;
    	long denom = this.denominator / gCD;
    	return new Lfraction(numer, denom);
    }
}

